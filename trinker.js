const { forEach } = require("./people");

module.exports = {
    title: function (){
        return `
$$$$$$$$\\        $$\\           $$\\                           $$\\                               
\\__$$  __|       \\__|          $$ |                          $$ |                              
   $$ | $$$$$$\\  $$\\ $$$$$$$\\  $$ |  $$\\  $$$$$$\\   $$$$$$\\  $$ | $$$$$$\\ $$\\    $$\\  $$$$$$\\  
   $$ |$$  __$$\\ $$ |$$  __$$\\ $$ | $$  |$$  __$$\\ $$  __$$\\ $$ |$$  __$$\\\\$$\\  $$  |$$  __$$\\ 
   $$ |$$ |  \\__|$$ |$$ |  $$ |$$$$$$  / $$$$$$$$ |$$ |  \\__|$$ |$$ /  $$ |\\$$\\$$  / $$$$$$$$ |
   $$ |$$ |      $$ |$$ |  $$ |$$  _$$<  $$   ____|$$ |      $$ |$$ |  $$ | \\$$$  /  $$   ____|
   $$ |$$ |      $$ |$$ |  $$ |$$ | \\$$\\ \\$$$$$$$\\ $$ |$$\\   $$ |\\$$$$$$  |  \\$  /   \\$$$$$$$\\ 
   \\__|\\__|      \\__|\\__|  \\__|\\__|  \\__| \\_______|\\__|\\__|  \\__| \\______/    \\_/     \\_______|
================================================================================================
   `.yellow
    },

    line: function(title = "="){
        return title.padStart(48, "=").padEnd(96, "=").bgBrightYellow.black
    },



    // ===============
    // === FILTERS ===
    // ===============


    // Fonction Nombre
    // entrée tableau p, le nom du filtre et le parametre du filtre
    // genere un tableau a partir de p et du parametre
    // return le nombre d'entrée dans le tableau
    nb:function(p,filter,parameter){
        let filtered= filter(p,parameter);
        return filtered.length
    },

    // TRI
    os: function(tab,by){

    },

    // == GENRE ==
    // entre un tableau de toutes les personnes et le parametre "genre" string
    // return un tableau de toutes les personnes qui sont du "genre"
    // exemple - console.log(nb(p,byGender,"Female"));
    byGender:function(i,gender){
        return i.filter(i=>i.gender===gender)
    },

    // == INTEREST ==
    // entre toutes les personnes et le parametre "looking for" H ou F en string
    // return un tableau toutes les personnes qui recherchent H ou F 
    byInterest: function(i,looking_for){
        return i.filter(i=>i.looking_for===looking_for)
    },
    
    // == MOVIE ==
    // entre le tableau de toutes les personnes et le parametre "pref movie" string
    // return un tableau de toutes les personnes qui aiment tel type de film
    byMovie:function(i,pref_movie){
        return i.filter((i=>i.pref_movie.includes(pref_movie)))
    },

    // == INCOME == 
    // entre le tableau de toutes les personnes et le parametre "income" string
    // return un tableau de toutes les personnes qui gagnent au dessus de "income"
    byIncomeMore: function(i,income){
        return i.filter(i=>(parseFloat(i.income.slice(1)))>=income)
    },
   
    // == BYMAIL ==
    // entre toutes les personnes  et le parametre "truc" string
    // return un tableau de toutes les personnes dont l'email contient "truc"
    byMail:function(i,email){
        return i.filter((i=>i.email.includes(email)))
    },
    
    // == ROUND TO 2 ==
    // entre des nombres décimaux string
    // return des nombres décimaux maxi 2 chiffres après la virgule string
    roundToTwo: function(num){
        return +(Math.round(num + "e+2")  + "e-2");
    },

    // ======  END FILTERS  ======


    // fonction 4K +
    // r=[],
    // for (let i of i){
    //     let iSimple{
    //         id: i.id,
    //         first_name: i.FN + i.LN,
    //         inc: i.income
    //     }
    //     r.push(iSimple)
    // }
    // return r

    // Tableau
    // i.sort((a,b)=>a.truc-b.truc)

    // Moyenne
    // i=[]
    // Acc=0
    // for (let i of i){
    //     acc=acc+i.income
    // },
    // moyenne=acc/this.i.length

    // Mediane
    // t=[]
    // t[t.length/2]+[(t.length/2)-1]







    // == MALE ==
    allMale: function(i){
        return i.filter(i=>i.gender==="Male")
    },
    nbOfMale: function(i){
        return this.allMale(i).length
    },

    // == FEMALE ==
    allFemale: function(i){
        return i.filter(i=>i.gender==="Female")
    },
    nbOfFemale: function(i){
        return this.allFemale(i).length;
    },

    // == MALE INTEREST ==
    allMaleInterest: function(i){
        return i.filter(i=>i.looking_for==="M")
    },
    nbOfMaleInterest: function(i){
        return this.allMaleInterest(i).length;
    },

    // == FEMALE INTEREST ==
    allFemaleInterest: function(i){
        return i.filter(i=>i.looking_for === "F")
    },
    nbOfFemaleInterest: function(i){
        return this.allFemaleInterest(i).length;
    },

    // == INCOME + 2000 == 
    allIncomeMoreTwoK: function(i){
        return i.filter(i => parseFloat(i.income.slice(1)) > 2000)
    },
    nbOfIncomeMoreTwoK: function(i){
        return this.allIncomeMoreTwoK(i).length;
    },

    // == DRAMA LOVERS == 
    allDramaLovers: function(i){
        return i.filter(i=>i.pref_movie.includes("Drama"))
    },
    nbOfDramaLovers: function(i){
        return this.allDramaLovers(i).length;
    },

    // == WOMEN LOVING SF == 
    allWsfLove: function(allFemale){
        return allFemale.filter(i => i.pref_movie.includes("Sci-Fi"))
    },
    nbOfWsfLove: function(i){
        return this.allWsfLove(i).length;
    },



    // ===============
    // === LEVEL 2 ===
    // ===============


    // == DOCUMENTARY LOVERS AND INCOME + $1482 == 
    allDocuInc: function(i){
        return i.filter(i => i.pref_movie.includes("Documentary") 
        && parseFloat(i.income.slice(1)) > 1482)
    },
    nbOfDocuInc: function(i){
        return this.allDocuInc(i).length;
    },

    // == HIGH INCOME LIST +4K (firstN, lastN, id, income) == 

    // nbOfHighIncome: function(i){
    //     return i.filter(i => parseFloat(i.income.slice(1)) > 4000).map(i => {
    //         return {
    //             Id: i.id,
    //             Nom: i.last_name,
    //             Prénom: i.first_name,
    //             Revenus: i.income
    //         }
    //     })
    // },

    allHighIncome: function(i){
        return i.filter(i => parseFloat(i.income.slice(1)) > 4000)
    },
    nbOfHighIncome: function(i){
        return this.allHighIncome(i).length;
    },

    // == Rich Man == 
    // entre le tableau généré par byGender
    // return la première entrée du nouveau tableau généré avec byGender et byIncome - string
    richestMan: function(i){
        let x = this.byGender(i, "Male");
        let money = [] ;
        for(let elem of x){
            temp = parseFloat(elem.income.slice(1))
            money.push([temp, elem.id, elem.last_name])
        }
        money.sort(function(a, b) {
            return a[0] - b[0];
          });
        return money[money.length-1]
    },


    // == AVERAGE SALARY == 
    // entre tous les income du tableau people
    // return une moyenne de tous les income string
    averageSalary: function(p){
        let totalSalary = 0;
        for(let i of p){
            totalSalary += parseFloat(i.income.slice(1));
        }
        let averageSalary = totalSalary / p.length;
        return this.roundToTwo(averageSalary);
    },

    // == MEDIAN SALARY == 
    // entre tous les income du tableau people
    // return le salaire median des income string
    // t=[]
    // t[t.length/2]+[(t.length/2)-1]
    medianSalary: function(i){
        let salary = i.map(i => parseFloat(i.income.slice(1))).sort((a, b) => a - b);
        let median = Math.floor(salary.length / 2);
        return this.roundToTwo((salary[median - 1] + salary[median]) / 2);
    },

    // == NORTHERN HEMISPHERE == 
    // entre les latitudes des gens
    // return le nombre des latitudes supérieures à 0 en string
    northHem: function(i){
        return i.filter(i => i.latitude >= 0).length;
    },

    // == SOUTHERN HEMISPHERE == 
    // entre les latitudes des gens
    // return le nombre des latitudes inférieures à 0 en string
    southhHem: function(i){
        return i.filter(i => i.latitude <= 0).length;
    },

    // == AVERAGE SALARY / SOUTH == 
    // entre le tableau des latitudes inférieures à 0 et leur income
    // return la moyenne des income des gens de l'hémisphère sud string
    averageSalarySouth: function(i){
        let southernPeople = i.filter(i => i.latitude < 0);
        let totalSalary = southernPeople.reduce((sum, i) => sum + parseFloat(i.income.slice(1)), 0);
        return this.roundToTwo(totalSalary / southernPeople.length);
    },


    // ===============
    // === LEVEL 3 ===
    // ===============

    // == NEAREST OF FIRST_NAME, LAST_NAME == 
    // entre le tableau des personnes, un nom et un prénom string
    // return la personne la plus proche de "nom" et "prénom" string

    // near: function(alice, bob){
    //     Math.sqrt(Math.pow(alice.lat-bob.lat,2)+(Math.pow(alice.long-bob.long,2)))
    // },
    nearestPerson: function(p, first_name, last_name){
        let nearest ={ first_name: "", id: "", distance: Infinity };
        let target = p.find(i => (i.first_name === first_name)&&(i.last_name === last_name));
        if (!target){
            return "person not found";
        }
        for (let i of p){
            let distance = Math.sqrt(Math.pow(target.latitude - i.latitude, 2) + Math.pow(target.longitude - i.longitude, 2));
            if (distance < nearest.distance && distance !== 0){
                nearest.first_name = i.first_name;
                nearest.id = i.id;
                (nearest.distance = distance);
            }
        }
        return nearest;
    },

    // == CLOSEST 10 == 
    // entre le tableau des personnes, un nom et un prénom string
    // return un tableau avec les 10 personnes les plus proches de "nom" et "prénom" 
    closestTen: function(p, first_name, last_name){
        let target = p.find(i => (i.first_name === first_name)&&(i.last_name === last_name));
        if (!target){
            return "person not found";
        }
        let closest = p.map(i =>{
            return{
                id: i.id,
                first_name: i.first_name,
                distance: Math.sqrt(Math.pow(target.latitude - i.latitude, 2) + Math.pow(target.longitude - i.longitude, 2))
            }
        }).sort((a, b) => a.distance - b.distance).slice(1, 11);
        return closest;
    },


    // == OLDEST == 
    // entre le tableau des personnes
    // return la personne la plus agé du tableau string
    oldest: function(people) {
        let oldest = people[0];
        for (let i = 1; i < people.length; i++) {
            if (new Date(people[i].date_of_birth) < new Date(oldest.date_of_birth)) {
                oldest = people[i];
            }
        }
        return oldest;
    },


    // == YOUNGEST == 
    // entre le tableau des personnes
    // return la personne la plus jeune du tableau string
    youngest: function(people) {
        let youngest = people[0];
        for (let i = 1; i < people.length; i++) {
            if (new Date(people[i].date_of_birth) > new Date(youngest.date_of_birth)) {
                youngest = people[i];
            }
        }
        return youngest;
    },

    // == AVERAGE AGE DIFFERENCE == 
    // entre le tableau des personnes
    // return un nombre, la moyenne de la difference d'age des gens du tableau string
    averageAgeDiff: function(people){
        let old = new Date(this.oldest(people).date_of_birth);
        let young = new Date(this.youngest(people).date_of_birth);
        let ageDifferences = people.map(person => {
            let personAge = new Date(person.date_of_birth);
            return (old - personAge) / (1000 * 60 * 60 * 24 * 365);
        });
        let sum = ageDifferences.reduce((a, b) => a - b);
        return this.roundToTwo(sum / ageDifferences.length);
    },
    
    
    // == FILM GENRE == 

    // people.pref_truc = "A,B,C";
    // let score = {};
    // forEach(people){
    //     let films=p.pref_truc.split("|");
    //     for (let i of films){
    //         if (!score[i]){
    //             score[i]=0
    //         }
    //         score[i]+=1;
    //     }
    // },







    match: function(i){
        return "not implemented".red;
    },
}